package android.bignerdranch.lv1mariotutic

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Half.toFloat
import android.view.View
import android.widget.*


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val editText1 = findViewById<EditText>(R.id.editText1)
        val editText2 = findViewById<EditText>(R.id.editText2)

        val textView1 = findViewById<TextView>(R.id.textView1)
        val textView2 = findViewById<TextView>(R.id.textView2)

        findViewById<Button>(R.id.button1).setOnClickListener {
            textView2.text = editText2.text
            textView1.text = editText1.text
        }

        val visina = findViewById<EditText>(R.id.visina)
        val tezina = findViewById<EditText>(R.id.tezina)
        var BMI=0.0
        val BMIText = findViewById<TextView>(R.id.BMI)
        findViewById<Button>(R.id.BMICalculate).setOnClickListener {
            BMI= Integer.parseInt(tezina.text.toString()).toDouble()/(Integer.parseInt(visina.text.toString()).toDouble())*Integer.parseInt(visina.text.toString()).toDouble()
            BMIText.text=BMI.toString()
        }

    }
}