package hr.ferit.ferit.todo.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.RecyclerView
import hr.ferit.ferit.todo.R
import hr.ferit.ferit.todo.data.DataSource

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val recyclerView =findViewById<RecyclerView>(R.id.recyclerView)
        val dataSource= DataSource().loadTasks()

        recyclerView.adapter=TaskAdapter(dataSource, this)

    }
}